#!/usr/bin/env python

import os
import copy
import pickle
import argparse
import logging
import logging.config
import time
import numpy as np
import torch
from torch.utils.data import DataLoader

import deepclean_prod as dc
from deepclean_prod import config
import glob

import gwpy
from gwpy.timeseries import TimeSeries, TimeSeriesDict

# Default tensor type
torch.set_default_tensor_type(torch.FloatTensor)

current_time = int(gwpy.time.tconvert("now"))

def parse_cmd():
    
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
    
    
    parser.add_argument('--offline', help='offline analysis of a longer segment. \
                        Only one iteration is performed', 
                        default=False, type=dc.io.str2bool)

    parser.add_argument('--live-stream', help='live (ie, running on a real-time data stream)\
                        If both offline and livestream are set to False, \
                        then frames of 1 s duration will be cleaned successively but not in real time', 
                        default=False, type=dc.io.str2bool)
    
    # Dataset arguments
    parser.add_argument('--clean-t0', help='GPS of the first sample', 
                        default= current_time , type=int)

    parser.add_argument('--clean-t1', help='GPS of the last sample. Needed for online analysis', 
                        default=0 , type=int)

    parser.add_argument('--clean-duration', default=4096.0 , type=float, 
                        help='Duration of data to be cleaned. Needed for offline analysis')

    parser.add_argument('--sliding-window-size', help='Duration of sliding window. \
                            Needed for online analysis.', type=int)
    
    parser.add_argument('--sliding-window-stride', help='Duration of active stride. \
                            Needed for online analysis.', type=int)

    parser.add_argument('--replay-offset', 
                        help='offset of replay from the current GPS time',
                        type=int)
    parser.add_argument('--fs', help='Sampling frequency', 
                        default=config.DEFAULT_SAMPLE_RATE, type=float)

    # Timeseries arguments
    parser.add_argument('--clean-kernel', help='Length of each segment in seconds', 
                        default=config.DEFAULT_CLEAN_KERNEL, type=float)
    parser.add_argument('--clean-stride', 
                        help='Stride between segments in seconds', 
                        default=config.DEFAULT_CLEAN_STRIDE, type=float)
    parser.add_argument('--pad-mode', help='Padding mode', 
                        default=config.DEFAULT_PAD_MODE, type=str)
    
    # Post-processing arguments
    parser.add_argument('--window', help='Window to apply to overlap-add',
                        default=config.DEFAULT_WINDOW, type=str)

    # Input/output arguments    
    parser.add_argument('--train-dir', nargs='+', default=[], 
                        help='lists of paths to the training directories')

    parser.add_argument('--checkpoint', nargs='+', default=[], 
                        help='Path to layer-wise model checkpoints')

    parser.add_argument('--ppr-file', nargs='+', default=[], 
                        help='Lists of paths to preprocessing settings for each layer')

    parser.add_argument('--hoft-path', default='.', 
                        help='Path to h(t) frames for layer 1', type = str)

    parser.add_argument('--detchar-path', help='Path to detchar frames', 
                        default='.', type=str)

    parser.add_argument('--hoft-frame-prefix', default='H-H1_HOFT',
                        help='h(t) frame prefixs excluding the start time and duration.', type = str)
    
    parser.add_argument('--detchar-frame-prefix', default='H-H1_Detchar',
                        help='detchar frame prefix excluding the start time and duration', type=str)
    
    parser.add_argument('--cleaned-frame-prefix',nargs='+', default=[],
                        help='List of cleaned h(t) frame prefixs excluding the start time and duration')
    
    parser.add_argument('--out-dir', help='Path to the directory to store cleaned frames ',
                        default='.', type=str)
    
    parser.add_argument('--out-channel', nargs='+', default=[], 
                        help='List of names of the output channel for each layer')
    
    parser.add_argument('--aggregation-latency', 
                        help='length of the excluded edge', default= 0, type=float)

    parser.add_argument('--log', help='Log file', 
                        default='./logs_dc_prod_clean_online.txt', type=str)
    
    parser.add_argument('--save-dataset', help='Save dataset', 
                        default=False, type=dc.io.str2bool)
    # cuda arguments
    parser.add_argument('--device', help='Device to use', 
                         default=config.DEFAULT_DEVICE, type=str)
    
    params = parser.parse_args()

    return params


def get_frames_list_continuous (frame_loc, prefix, 
                                start_time, end_time, 
                                frame_length = 1):
    '''
    make a list of all frames of given length between 
    a start and end times. 
    '''
    frames = []
    for i in np.arange (start_time, end_time+frame_length, frame_length):
        fname = os.path.join(frame_loc, "{}-{:d}-{:d}.gwf".format(
                            prefix, i, int(frame_length)) )
        frames.append(fname)
    return frames

def read_data (params, fname_hoft, fname_detchar, target_channel, wit_channels, 
               layer = 0, hoft_data = None):

    t0 = params.clean_t0
    duration = params.clean_duration
    
    data = dc.timeseries.TimeSeriesSegmentDataset(
        params.clean_kernel, params.clean_stride, params.pad_mode)

    wit_channels, fake_chans = data.categorize_channels (wit_channels)

    if hoft_data is None:
        frames  = TimeSeriesDict.read(fname_hoft,    channels = target_channel, 
                        start = t0, end = t0 + duration)
    else:
        frames  = TimeSeriesDict()
        frames[target_channel[0]] = hoft_data
    
    detchar = TimeSeriesDict.read(fname_detchar, channels = wit_channels,
                        start = t0, end = t0 + duration)
    frames.append(detchar)
    
    if len(fake_chans) > 0:
        ## t0 and duration from the real frames

        frames = data.add_fake_sinusoids (frames, fake_chans, 
                                          t0 = t0, 
                                          duration = duration, fs = params.fs)
    
    frames = frames.resample(params.fs)

    # sorted by channel name
    frames = dc.timeseries.OrderedDict(sorted(frames.items()))
    
    # reset attributes 
    dataset = []
    chans = []
    for chan, ts in frames.items():
        dataset.append(ts.value)
        chans.append(chan)
    dataset = np.stack(dataset)
    chans = np.stack(chans)

    data.data = dataset
    data.channels = chans
    data.t0 = t0
    data.fs = params.fs
    data.target_idx = np.where(data.channels == target_channel)[0][0] 
    return data



def update_data (data, params, latest_frame_time, channels, layer = 0, hoft_data = None):
    
    if hoft_data is None:
        latest_frame_hoft = os.path.join(params.hoft_path[layer], "{}-{:d}-{:d}.gwf".format(
                params.hoft_frame_prefix[layer], latest_frame_time, 1) )
        data_latest    = TimeSeriesDict.read(latest_frame_hoft,    channels = channels[:1])
    else:
        data_latest = hoft_data
    
    latest_frame_detchar = os.path.join(params.detchar_path, "{}-{:d}-{:d}.gwf".format(
                params.detchar_frame_prefix, latest_frame_time, 1) )
    detchar_latest = TimeSeriesDict.read(latest_frame_detchar, channels = channels[1:])
    data_latest.append(detchar_latest) 
    data_latest = data_latest.resample(params.fs)
    # sorted by channel name
    data_latest = dc.timeseries.OrderedDict(sorted(data_latest.items()))

    # reset attributes 
    dataset_latest = []
    for chan, ts in data_latest.items():
        dataset_latest.append(ts.value)
    dataset_latest = np.stack(dataset_latest)

    dataset = np.concatenate((data.data[:,int(data.fs):], dataset_latest), axis = 1)
    data.data = dataset
    data.t0 += 1
    return data


def load_model (train_dir, n_witchans, checkpoint = None):

    # Creating model and load from checkpoint
    model = dc.nn.net.DeepClean(n_witchans)
    model = model.to(device)  # transfer model to GPU if available
    logging.info(model)

    if checkpoint is None:
        checkpoint = dc.nn.utils.get_last_checkpoint(
            os.path.join(train_dir, 'models'))
    logging.info('Loading model from checkpoint: {}'.format(checkpoint))
    model.load_state_dict(torch.load(checkpoint, map_location=device))
    return model


def preprocess_and_predict (data, params, model, ppr, device, batch_size):
    preprocessed = data.bandpass(ppr['filt_fl'], ppr['filt_fh'], 
                                 ppr['filt_order'], 'target')
    preprocessed = preprocessed.normalize(ppr['mean'], ppr['std'])

    # Load preprocessed data into dataloader
    data_loader = DataLoader(
        preprocessed, batch_size=batch_size,
        num_workers=config.DEFAULT_NUM_WORKERS, shuffle=False)

    target = data.get_target()  # get raw data

    # predict noise from auxilliary channels
    pred_batches = dc.nn.utils.evaluate(data_loader, model, device=device)

    # post-processing the prediction
    # overlap-add to join together prediction
    noverlap = int((preprocessed.kernel - preprocessed.stride) * preprocessed.fs)
    pred = dc.signal.overlap_add(pred_batches, noverlap, params.window)
    # convert back to unit of target
    pred *= ppr['std'][preprocessed.target_idx].ravel()
    pred += ppr['mean'][preprocessed.target_idx].ravel()
    # apply bandpass filter
    pred = dc.signal.filter_add(pred, preprocessed.fs, ppr['filt_fl'], ppr['filt_fh'],
                                ppr['filt_order'])
    # because of data padding, we apply a cut to the output
    pred = pred[:target.size]
    return pred
    


params =  parse_cmd()
pickle.dump({"params": params}, open("dc_online_params.p", "wb"))
params = pickle.load(open("dc_online_params.p", "rb"))['params']

# Use GPU if available
device = dc.nn.utils.get_device(params.device)

nlayers = len(params.train_dir)

if params.checkpoint == []:
    params.checkpoint = [None] * nlayers

if params.ppr_file == []:
    params.ppr_file = [None] * nlayers
    
if not os.path.exists(params.out_dir):
    os.makedirs(params.out_dir)

ppr_list = []
for layer in range(nlayers):
    if params.ppr_file[layer] is None:
        params.ppr_file[layer] = os.path.join(params.train_dir[layer], 'ppr.bin')
    ppr = pickle.load(open(params.ppr_file[layer], 'rb'))
    ppr_list.append(ppr)

model_list = []
for layer in range(nlayers):

    target_channel = [ppr_list[layer]['target']]
    wit_channels   = ppr_list[layer]['wit_channels']
    n_witchans = ppr_list[layer]['n_witness_channels']
    model = load_model (params.train_dir[layer], n_witchans,
                             checkpoint = params.checkpoint[layer])
    model_list.append(model)

####################################################
if params.offline:
    
    batch_size = config.DEFAULT_BATCH_SIZE
    
    frame_string = "{}-*-*.gwf".format(params.hoft_frame_prefix)
    fname_hoft_initial_window = glob.glob(os.path.join(params.hoft_path, frame_string))

    frame_string = "{}-*-*.gwf".format(params.detchar_frame_prefix)
    fname_detchar_initial_window = glob.glob(os.path.join(params.detchar_path, frame_string))

    for layer in range(nlayers):
        
        channels = ppr_list[layer]['channels']    
        target_channel = [ppr_list[layer]['target']]
        wit_channels   = ppr_list[layer]['wit_channels']
        n_witchans = ppr_list[layer]['n_witness_channels']
        
        if layer == 0:
            data = read_data (params, fname_hoft_initial_window, 
                fname_detchar_initial_window, target_channel, wit_channels)
        else:
            data = read_data (params, fname_hoft_initial_window,
                fname_detchar_initial_window, target_channel, wit_channels,
                layer = layer, hoft_data = cleaned_ts)

        pred = preprocess_and_predict (data, params, model_list[layer], ppr_list[layer], device, batch_size)

        # subtract noise prediction from raw data
        target = data.get_target()  # get raw data
        clean = target - pred
        cleaned_ts = TimeSeries(clean, t0=data.t0, sample_rate=data.fs, 
                channel=params.out_channel[layer], name=params.out_channel[layer])

        cleaned_frame_name   = "{}-{:d}-{:d}.gwf".format(
            params.cleaned_frame_prefix[layer], int(data.t0), int(cleaned_ts.duration.value))
        cleaned_ts.write(os.path.join(params.out_dir, cleaned_frame_name))
        logging.info ("Writing DeepClean frame {} . .".format(cleaned_frame_name))
    
#####################################  end of offline analysis  ###############    
    
if not params.offline:

    if params.live_stream:
        #current_time = int(gpstime.gps_time_now ())
        '''current time is the time of the most recent frame, not the current GPS time 
        the replay frames are found to be consistently offset by a few seconds 
        w.r.t the current GPS time. So we account for this difference'''
        current_time        = int(gwpy.time.tconvert("now")) - params.replay_offset  
        params.clean_t0     = current_time 
        initial_window_start= params.clean_t0 - params.sliding_window_size + 1
        initial_window_end  = params.clean_t0 
    else:
        initial_window_start= params.clean_t0  
        initial_window_end  = params.clean_t0 + params.sliding_window_size - 1

    ########################################################################
    if params.clean_t1 == 0:
        max_iteration = int((7 * 24 * 3600)/params.sliding_window_stride)
    else:
        max_iteration = int((params.clean_t1 - params.clean_t0)/params.sliding_window_stride)

    ########################################################################
    #####  Load data, channel list, t0, fs etc
    
    # reading initial data for all layers
    data_list = []
    for layer in range(nlayers):

        channels = ppr_list[layer]['channels']    
        target_channel = [ppr_list[layer]['target']]
        wit_channels   = ppr_list[layer]['wit_channels']
        n_witchans = ppr_list[layer]['n_witness_channels']

        fname_hoft_initial_window = get_frames_list_continuous (
                 frame_loc = params.hoft_path[layer], 
                 prefix= params.hoft_frame_prefix[layer], 
                 start_time= initial_window_start, 
                 end_time  = initial_window_end, 
                 frame_length = 1)

        fname_detchar_initial_window = get_frames_list_continuous (
                 frame_loc = params.detchar_path, 
                 prefix= params.detchar_frame_prefix, 
                 start_time= initial_window_start, 
                 end_time  = initial_window_end, 
                 frame_length = 1)
    ########################################################################

    ff = open(params.log, 'w')
    ff.write(f"# Starting DeepClean-online multi-layer . .")
    ff.flush()

    ########################################################################
    latest_frame_time = initial_window_end + 1 
    data = {}
    for iteration in range (max_iteration):
        
        begin = time.time()
        
        for layer in range(nlayer):
    
            channels       = ppr_list[layer]['channels']    
            target_channel = [ppr_list[layer]['target']]
            wit_channels   = ppr_list[layer]['wit_channels']
            n_witchans     = ppr_list[layer]['n_witness_channels']

            if iteration == 0:
                if layer == 0:
                    data[str(layer)] = read_data (params, fname_hoft_initial_window, 
                              fname_detchar_initial_window, target_channel, wit_channels)
                else:
                    data[str(layer)] = read_data (params, fname_hoft_initial_window, 
                              fname_detchar_initial_window, target_channel, wit_channels,
                               layer = layer, hoft_data = cleaned_ts)

            else:
                # update data: remove one second from bottom end, and add new one sec at the top
                if layer == 0:
                    data[str(layer)] = update_data (data[str(layer)], params, latest_frame_time, 
                                                    channels, layer = layer)
                else:
                    data[str(layer)] = update_data (data[str(layer)], params, latest_frame_time, 
                                                    channels, layer = layer, hoft_data = cleaned_ts)

            pred = preprocess_and_predict (data[str(layer)], 
                        params, model_list[layer], ppr_list[layer], device)

            # subtract noise prediction from raw data
            target = data[str(layer)].get_target()  # get raw data
            clean = target - pred
            cleaned_ts = TimeSeries(clean, t0=data[str(layer)].t0, sample_rate=data[str(layer)].fs, 
                    channel=params.out_channel[layer], name=params.out_channel[layer])

            crop_from = latest_frame_time - params.aggregation_latency
            crop_till = crop_from + 1
            cleaned_ts_cropped = cleaned_ts.crop(crop_from, crop_till)

            cleaned_frame_name   = "{}-{:.2f}-{:d}.gwf".format(
                                  params.cleaned_frame_prefix[layer], round(crop_from, 2), 1)

            cleaned_ts_cropped.write(os.path.join(params.out_dir, cleaned_frame_name))
            logging.info ("Writing DeepClean frame {} . .".format(cleaned_frame_name))
            print ("layer {:d} completed".format(layer+1))

        ## in the live streaming, wait until the new set of frames become available
        stop = time.time()
        elapsed = stop - begin
        wait_time    = 1 - elapsed
        if (params.live_stream == True) and (wait_time > 0):
            time.sleep(wait_time)

        ff = open(params.log, 'a')
        ff.write("Completed at {:d}, time elapsed {:.4f} \n".format(
            int(gwpy.time.tconvert("now")), elapsed))
        ff.flush()

        latest_frame_time += 1


