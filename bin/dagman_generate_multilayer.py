
import os
import sys
import argparse
import subprocess
import random
import secrets
import pickle
import numpy as np
from deepclean_prod import io

# Parse command line argument
def parse_cmd():
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
    parser.add_argument('config', help='Path to config file', type=str) 
    parser.add_argument('--submit', help='Submit DAGMAN', action='store_true')
    params = parser.parse_args()
    return params

params = parse_cmd()
pickle.dump({"params": params}, open("dagman_gen_args.p", "wb"))
params = pickle.load(open("dagman_gen_args.p", "rb"))['params']

# Parse config file
config = io.parse_config(params.config, 'config')

nlayers = int(config['nlayers'])

#if not os.getcwd() in config['out_dir']:
#    config['out_dir'] = os.path.join(os.getcwd(), config['out_dir'])


out_dir  = config['out_dir']
prefix   = config.get('prefix', 'prefix')
job_name = config.get('job_name', 'job')

# name extended by a random number, to avoid repeataion
job_name_extended = job_name + "_" + secrets.token_hex(8)
max_clean_duration = int(config.get('max_clean_duration', 4096))


request_memory_getdata  = config.get('request_memory_getdata', '1 GB')
request_memory_training = config.get('request_memory_training', '2 GB')
request_memory_cleaning = config.get('request_memory_cleaning', '2 GB')
request_memory_postproc = config.get('request_memory_postproc', '1 GB')


dcprodgetdata  = config.get('dcprodgetdata',  os.popen('which dc-prod-getdata').read().strip('\n'))
dcprodtrain    = config.get('dcprodtrain',    os.popen('which dc-prod-train-multilayer').read().strip('\n'))
dcprodclean    = config.get('dcprodclean',    os.popen('which dc-prod-clean-multilayer').read().strip('\n'))
dcprodpostproc = config.get('dcprodpostproc', os.popen('which dc-prod-postproc').read().strip('\n'))


# Create output directory
out_dir_log    = os.path.join(out_dir,'condor','logs')
out_dir_submit = os.path.join(out_dir,'condor','submit')
#out_dir_submit = './'

os.makedirs(out_dir_log,    exist_ok=True)
os.makedirs(out_dir_submit, exist_ok=True)


## getdata.sub content
getdata_sub = '''
universe = vanilla
executable = '''+dcprodgetdata+'''
request_memory = '''+request_memory_getdata+'''
getenv = True
log    = '''+out_dir+'''/condor_logs/getdata_$(ID).log
output = '''+out_dir+'''/condor_logs/getdata_$(ID).out
error  = '''+out_dir+'''/condor_logs/getdata_$(ID).err
accounting_group = '''+config['accounting_group']+'''
request_disk = 1 GB
stream_output = True
stream_error = True
arguments = $(ARGS)
queue
'''

## train.sub content
train_sub = '''
universe = vanilla
executable = '''+dcprodtrain+'''
request_memory = '''+request_memory_training+'''
getenv = True
requirements = (CUDACapability > 3.5)
log    = '''+out_dir+'''/condor_logs/train_$(ID).log
output = '''+out_dir+'''/condor_logs/train_$(ID).out
error  = '''+out_dir+'''/condor_logs/train_$(ID).err
accounting_group = '''+config['accounting_group']+'''
request_gpus = 1
request_disk = 1 GB
stream_output = True
stream_error = True
arguments = $(ARGS)
queue
'''

clean_sub = '''
universe = vanilla
executable = '''+dcprodclean+'''
request_memory = '''+request_memory_cleaning+'''
getenv = True
requirements = (CUDACapability > 3.5)
log    = '''+out_dir+'''/condor_logs/clean_$(ID).log
output = '''+out_dir+'''/condor_logs/clean_$(ID).out
error  = '''+out_dir+'''/condor_logs/clean_$(ID).err
accounting_group = '''+config['accounting_group']+'''
request_gpus = 1
request_disk = 1 GB
stream_output = True
stream_error = True
arguments = $(ARGS)
queue
'''
## postproc.sub content
postproc_sub = '''
universe = vanilla
executable = '''+dcprodpostproc+'''
request_memory = '''+request_memory_postproc+'''
getenv = True
log    = '''+out_dir+'''/condor_logs/postproc_$(ID).log
output = '''+out_dir+'''/condor_logs/postproc_$(ID).out
error  = '''+out_dir+'''/condor_logs/postproc_$(ID).err
accounting_group = '''+config['accounting_group']+'''
request_disk = 1 GB
stream_output = True
stream_error = True
arguments = $(ARGS)
queue
'''

def get_getdata_ARGS (config, getdata_config):
    
    # make a list of all t0 and duration 
    # if train segment is between the clean segments, then no need to include separately

    arg = ""

    arg += " --t0 "
    arg += getdata_config['t0']

    arg += " --duration "
    arg += getdata_config['duration']

    arg += " --chanslist "
    arg += config['chanslist'].replace("," , " ")

    arg += " --fs "
    arg += config['fs']

    arg += " --out-dir "
    arg += getdata_config['out_dir']

    arg += " --hoft-frame-prefix "
    arg += config['hoft_frame_prefix']

    arg += " --detchar-frame-prefix "
    arg += config['detchar_frame_prefix']

    return arg


def get_train_ARGS (config, train_config):

    arg = ""

    arg += " --fs "
    arg += config['fs']

    arg += " --chanslist " 
    arg += config['chanslist'].replace("," , " ")

    arg += " --train-kernel "
    arg += config['train_kernel'] 

    arg += " --train-stride "
    arg += config['train_stride']

    arg += " --pad-mode " 
    arg += config['pad_mode'] 

    arg += " --filt-fl " 
    arg += config['filt_fl'].replace("," , " ") 

    arg += " --filt-fh " 
    arg += config['filt_fh'].replace("," , " ") 

    arg += " --filt-order " 
    arg += config['filt_order'] 

    arg += " --device " 
    arg += config['device']

    arg += " --train-frac " 
    arg += config['train_frac'] 

    arg += " --batch-size " 
    arg += config['batch_size'] 

    arg += " --max-epochs " 
    arg += config['max_epochs'] 

    arg += " --num-workers "
    arg += config['num_workers'] 

    arg += " --lr " 
    arg += config['lr'] 

    arg += " --weight-decay " 
    arg += config['weight_decay'] 

    arg += " --fftlength " 
    arg += config['fftlength'] 

    arg += " --psd-weight " 
    arg += config['psd_weight'] 

    arg += " --mse-weight " 
    arg += config['mse_weight'] 

    arg += " --train-dir " 
    arg += train_config['train_dir'].replace("," , " ") 

    arg += " --train-t0 " 
    arg += str(train_config['train_t0']) 

    arg += " --train-duration " 
    arg += str(train_config['train_duration']) 
    
    arg += " --cross-psd-weight "
    arg += config['cross_psd_weight'] 

    arg += " --edge-weight "
    arg += config['edge_weight'] 

    arg += " --edge-frac "
    arg += config['edge_frac'] 

    arg += " --hoft-path "
    arg += train_config['hoft_path']

    arg += " --detchar-path "
    arg += train_config['detchar_path']

    arg += " --hoft-frame-prefix "
    arg += config['hoft_frame_prefix']

    arg += " --detchar-frame-prefix "
    arg += config['detchar_frame_prefix']

    arg += " --out-channel "
    arg += config['out_channel'].replace("," , " ") 
    
    return arg


def get_clean_ARGS (config, clean_config):
    
    arg = ""
    arg += " --save-dataset "  
    arg += config['save_dataset']

    #arg += " --load-dataset "  
    #arg += config['load_dataset']

    arg += " --fs "
    arg += config['fs']

    arg += " --out-dir " 
    arg += clean_config['out_dir']

    arg += " --out-channel " 
    arg += config['out_channel'].replace("," , " ")

    arg += " --clean-kernel "
    arg += config['clean_kernel'] 

    arg += " --clean-stride "
    arg += config['clean_stride']
    
    arg += " --pad-mode " 
    arg += config['pad_mode'] 

    arg += " --window " 
    arg += config['window'] 

    arg += " --device " 
    arg += config['device']

    arg += " --train-dir " 
    arg += clean_config['train_dir'].replace("," , " ")

    arg += " --clean-t0 " 
    arg += str(clean_config['clean_t0'])

    arg += " --clean-duration " 
    arg += str(clean_config['clean_duration'])

    if not io.str2bool(config['offline']):
        arg += " --clean-t1 "
        arg += str(clean_config['clean_t1'])

        arg += " --log "
        arg += config['log']
            
    arg += " --live-stream "
    arg += config['live_stream']

    arg += " --offline "
    arg += config['offline']

    arg += " --sliding-window-size "
    arg += config['sliding_window_size']

    arg += " --sliding-window-stride "
    arg += config['sliding_window_stride']

    arg += " --replay-offset "
    arg += config['replay_offset']

    arg += " --hoft-path "
    arg += clean_config['out_dir']

    arg += " --detchar-path "
    arg += clean_config['out_dir']

    arg += " --hoft-frame-prefix "
    arg += config['hoft_frame_prefix']

    arg += " --detchar-frame-prefix "
    arg += config['detchar_frame_prefix']

    arg += " --cleaned-frame-prefix "
    arg += config['cleaned_frame_prefix'].replace("," , " ")

    arg += " --aggregation-latency "
    arg += config['aggregation_latency']

    
    return arg


def get_postproc_ARGS (config, postproc_config, clean_config):
    
    arg = ""
    
    arg += " --nlayers "  
    arg += config['nlayers']
    
    arg += " --out-dir "  
    arg += clean_config['out_dir']
    
    arg += " --out-channel "  
    arg += config['out_channel']
    
    arg += " --ifo "  
    arg += config['ifo']
    
    arg += " --t0 "  
    arg += postproc_config['t0']
    
    arg += " --t1 "  
    arg += postproc_config['t1']

    arg += " --chanslist "  
    arg += config['chanslist']
    
    arg += " --filt-fl "  
    arg += config['filt_fl']
    
    arg += " --filt-fh "  
    arg += config['filt_fh']
    
    arg += " --train-dir "  
    arg += config['train_dir']
    
    arg += " --fftlength-spec "  
    arg += config['fftlength_spec']
    
    arg += " --fftlength-asd "  
    arg += config['fftlength_asd']
    
    arg += " --asd-min "  
    arg += config['asd_min']
    
    arg += " --asd-max "  
    arg += config['asd_max']
    
    arg += " --asd-whiten-min "  
    arg += config['asd_whiten_min']
    
    arg += " --asd-whiten-max "  
    arg += config['asd_whiten_max']
    
    arg += " --pp-basedir "  
    arg += config['pp_basedir']
    
    arg += " --asdr-duration "  
    arg += config['asdr_duration']
    
    arg += " --offline "  
    arg += config['offline']
    
    arg += " --hoft-path "  
    arg += postproc_config['hoft_path']
    
    arg += " --hoft-frame-prefix "  
    arg += config['hoft_frame_prefix']
    
    arg += " --cleaned-frame-prefix "  
    arg += config['cleaned_frame_prefix']
    
    arg += " --train-dir "  
    arg += clean_config['train_dir']

    return arg


# Run segment script if not already provided
if 'segment_file' not in config.keys():
    fetch_segments = True
    config['segment_file']    = os.path.join(out_dir, 'segment.txt')

elif not os.path.exists(config['segment_file']):
    fetch_segments = True
    if not os.getcwd() in config['segment_file']:
        config['segment_file'] = os.path.join(os.getcwd(), config['segment_file'])    
else:
    fetch_segments = False

if fetch_segments:
    segment_cmd = 'dc-prod-segment ' + io.dict2args(config, 
                        ('t0', 't1', 'train_duration', 'train_cadence', 
                         'start_training_after', 'ifo', 'segment_file'))
    print('Get segment data')
    subprocess.check_call(segment_cmd.split(' '))
    
# Read in segment data
segment_data = np.genfromtxt(config['segment_file'])

dag_script = ''''''
interjob_dependence = ''''''

dag_sh = ''''''

getdata_submit_filename = 'deepClean_getdata_{}.sub'.format(job_name)
train_submit_filename = 'deepClean_train_{}.sub'.format(job_name)
clean_submit_filename = 'deepClean_clean_{}.sub'.format(job_name)
postproc_submit_filename = 'deepClean_postproc_{}.sub'.format(job_name)
dag_submit_filename   = 'deepClean_dag_{}.dag'.format(job_name)
dag_sh_filename       = 'deepClean_dag_{}.sh'.format(job_name)

# dag_id will be used as extentions for job names within the dag script
dag_id = 0



for seg in segment_data:
    
    # Get training/cleaning time
    train_t0, train_t1, clean_t0, clean_t1, chunk_idx = seg.astype(int)
    
    ################  some weird conditional statements to adjust the training duration 
    ##  in case it is not sufficiently long
    
    if int(config['t1']) < clean_t0 :
        current_segment_starts_after_requested_t1 = True
        #break
    elif int(config['t1']) < clean_t1:
        clean_t1 = int(config['t1'])
    clean_duration = clean_t1 - clean_t0

    if train_t1 > clean_t1: # shouldn't happen given the way we have chosen segs
        train_t1 =  clean_t1 
    train_duration = train_t1 - train_t0
        
    ###################################################################################
        
    # Get directory for segment        
    segment_subdir = os.path.join(out_dir, '{}-{:d}-{:d}'.format(
        prefix, clean_t0, clean_duration))


    # job identifiers within the dag script
    getdata_job  = 'getdata_' +job_name_extended+'_'+str(dag_id)
    train_job    = 'train_'   +job_name_extended+'_'+str(dag_id)
    clean_job    = 'clean_'   +job_name_extended+'_'+str(dag_id)
    postproc_job = 'postproc_'+job_name_extended+'_'+str(dag_id)
        
    # Set up getdata job 
    t0 = np.arange(clean_t0, clean_t1, max_clean_duration)

    dag_script += "\n\n#Jobs for data between GPS times {:d} and {:d}\n".format(clean_t0, clean_t1)
    dag_script += "#----------------------------------------------------------------------\n"

    dag_sh     += "\n\n#Executables for data between GPS times {:d} and {:d}\n".format(clean_t0, clean_t1)
    dag_sh     += "#----------------------------------------------------------------------\n"

    
    if io.str2bool(config['download_data']):

        getdata_config = {}
        getdata_config['out_dir'] = segment_subdir
        getdata_config['t0']  = ""
        getdata_config['duration'] = ""
        for i in range(len(t0)):
            getdata_config['t0'] += str(t0[i]) + " "
            if t0[i] + max_clean_duration > clean_t1:
                duration = clean_t1 - t0[i]
            else:
                duration = max_clean_duration
            getdata_config['duration'] += str(duration) + " "       


        # generate argument string for getdata job
        getdata_arg = get_getdata_ARGS (config, getdata_config)
        dc_prod_getdata_sh = "dc-prod-getdata {}\n".format(getdata_arg)

        # dag entry for the getdata job 
        cmd_job_getdata   =  '''JOB {} {}'''.format(
                                    getdata_job, os.path.join(out_dir_submit, getdata_submit_filename))
        cmd_retry_getdata = '''RETRY {} {:d}'''.format(getdata_job, 10)
        cmd_var_getdata   = '''VARS {} ARGS="{}" ID="{:d}" '''.format(getdata_job, getdata_arg, dag_id)

        dag_script += cmd_job_getdata   + '\n' 
        dag_script += cmd_retry_getdata + '\n' 
        dag_script += cmd_var_getdata   + '\n\n' 

        dag_sh     += dc_prod_getdata_sh + '\n'       
        
        
    # Set up training job if use_pretrained_model = False 
    if not io.str2bool(config['use_pretrained_model']):
        
        segment_train_dir = ""
        for layer in range(nlayers):
            segment_train_dir += os.path.join(segment_subdir, "train_layer_{:d}".format(layer+1)) + " "
            
        train_config = {}
        train_config['train_dir']      = segment_train_dir
        train_config['train_t0']       = train_t0
        train_config['train_duration'] = train_duration
        train_config['hoft_path']      = segment_subdir
        train_config['detchar_path']   = segment_subdir

        # generate argument string for training job
        train_arg = get_train_ARGS (config, train_config)
        dc_prod_train_sh = "dc-prod-train-multilayer {}\n".format(train_arg)

        # dag entry for the training job 
        cmd_job_train   =  '''JOB {} {}'''.format(train_job, os.path.join(out_dir_submit, train_submit_filename))
        cmd_retry_train = '''RETRY {} {:d}'''.format(train_job, 10)
        cmd_var_train   = '''VARS {} ARGS="{}" ID="{:d}" '''.format(train_job, train_arg, dag_id)

        dag_script += cmd_job_train   + '\n' 
        dag_script += cmd_retry_train + '\n' 
        dag_script += cmd_var_train   + '\n\n' 

        dag_sh     += dc_prod_train_sh + '\n' 

        if io.str2bool(config['download_data']):
            interjob_dependence += 'Parent '+getdata_job+ ' Child '+train_job + '\n'

        
    # Set up cleaning jobs (could be more than 1)
    clean_config = {}
    clean_config['out_dir']   = segment_subdir
    if io.str2bool(config['use_pretrained_model']):
        clean_config['train_dir'] = config['train_dir']
    else:
        clean_config['train_dir'] = segment_train_dir
    
    # if the active segment length > max_clean_duration, then split as multiple segments 
    for i in range(len(t0)):
        
        # job name further extended for <i>th clean job parented by <dag_id>th train job
        clean_job_i = '{}_{:d}'.format(clean_job,i)
        # 
        clean_config['clean_t0'] = t0[i]
        if t0[i] + max_clean_duration > clean_t1:
            duration = clean_t1 - t0[i]
        else:
            duration = max_clean_duration
        clean_config['clean_duration'] = duration        
        
        # generate argument string for cleaning job
        clean_arg = get_clean_ARGS (config, clean_config)
        dc_prod_clean_sh = "dc-prod-clean-multilayer {}\n".format(clean_arg)
        
        cmd_job_clean   = '''JOB {} {}'''.format(clean_job_i, os.path.join(out_dir_submit, clean_submit_filename))
        cmd_retry_clean = '''RETRY {} {:d}'''.format(clean_job_i, 5)

        cmd_var_clean   = '''VARS {} ARGS="{}" ID="{:d}_{:d}" '''.format(clean_job_i, clean_arg, dag_id, i)

        dag_script += cmd_job_clean   + '\n'
        dag_script += cmd_retry_clean + '\n'
        dag_script += cmd_var_clean   + '\n\n'
        
        dag_sh     += dc_prod_clean_sh + "\n"
        
        if io.str2bool(config['download_data']):
            interjob_dependence += 'Parent '+getdata_job+ ' Child '+clean_job_i + '\n'
    
        if not io.str2bool(config['use_pretrained_model']):
            interjob_dependence += 'Parent '+train_job+ ' Child '+clean_job_i + '\n'
            
            
    postproc_config = {}
    postproc_config['t0'] = str(clean_t0)
    postproc_config['t1'] = str(clean_t1)
    postproc_config['hoft_path']      = segment_subdir

    # generate argument string for training job
    postproc_arg = get_postproc_ARGS (config, postproc_config, clean_config)
    dc_prod_postproc_sh = "dc-prod-postproc {}\n".format(postproc_arg)

    # dag entry for the training job 
    cmd_job_postproc   =  '''JOB {} {}'''.format(
                postproc_job, os.path.join(out_dir_submit, postproc_submit_filename))
    cmd_retry_postproc = '''RETRY {} {:d}'''.format(postproc_job, 10)
    cmd_var_postproc   = '''VARS {} ARGS="{}" ID="{:d}" '''.format(postproc_job, postproc_arg, dag_id)

    dag_script += cmd_job_postproc   + '\n' 
    dag_script += cmd_retry_postproc + '\n' 
    dag_script += cmd_var_postproc   + '\n\n' 

    dag_sh     += dc_prod_postproc_sh + '\n' 

    for i in range(len(t0)):
        clean_job_i = '{}_{:d}'.format(clean_job,i)
        interjob_dependence += 'Parent '+clean_job_i+ ' Child '+postproc_job + '\n'
    
    dag_id += 1
    
    
dag_script += '\n\n'
dag_script += interjob_dependence

# write submit files
text_file = open(os.path.join(out_dir_submit, train_submit_filename), "w")
text_file.write(train_sub)
text_file.close()

text_file = open(os.path.join(out_dir_submit, clean_submit_filename), "w")
text_file.write(clean_sub)
text_file.close()

text_file = open(os.path.join(out_dir_submit, dag_submit_filename), "w")
text_file.write(dag_script)
text_file.close()

text_file = open(os.path.join(out_dir_submit, dag_sh_filename), "w")
text_file.write(dag_sh)
text_file.close()

cmd_dag_submit = 'condor_submit_dag {}'.format(os.path.join(os.getcwd(), out_dir_submit, dag_submit_filename))

if params.submit:
    os.system(cmd_dag_submit)
else:
    print ('Dag generated successfully . .\n')
    print ('To submit the dag, run the following command from terminal:\n\t{}\n'.format(cmd_dag_submit))

