#!/usr/bin/env python

import os
import copy
import pickle
import argparse
import logging


import torch
from torch.utils.data import DataLoader

import deepclean_prod as dc
from deepclean_prod import config

from gwpy.timeseries import TimeSeries

# Default tensor type
torch.set_default_tensor_type(torch.FloatTensor)


def parse_cmd():
    
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
    
    # Dataset arguments
    parser.add_argument('--t0', nargs='+', default=[], help='GPS of the first sample', type=int)
    parser.add_argument('--duration', nargs='+', default=[], help='Duration of frame', type=int)
    parser.add_argument('--chanslist', nargs='+', default=[], help='Lists of paths to the channel lists')
    parser.add_argument('--fs', help='Sampling frequency',  default=None, type=float)
    parser.add_argument('--out-dir', help='Path to the output directory', default='.', type=str)
    parser.add_argument('--hoft-frame-prefix', default='H-H1_HOFT',
                help='h(t) frame prefixs excluding the start time and duration.', type = str)
    parser.add_argument('--detchar-frame-prefix', 
                help='detchar frame prefix excluding the start time and duration', default='H-H1_Detchar', type=str)

    params = parser.parse_args()

    return params

params =  parse_cmd()
pickle.dump({"params": params}, open("dc_get_data_params.p", "wb"))
params = pickle.load(open("dc_get_data_params.p", "rb"))['params']


if len(params.t0) == len(params.duration):
    nframes = len(params.t0)
else:
    raise ValueError(" 't0' and 'duration' should be lists of equal lengths . .")

# Create output directory
if params.out_dir is not None:
    os.makedirs(params.out_dir, exist_ok=True)


witness_channels_all_layers = []
for layer in range(len(params.chanslist)):
    channels = open(params.chanslist[layer]).read().splitlines()
    if layer == 0:
        target_channel = channels[:1]
    witness_channels_all_layers += channels[1:]

for i in range(nframes):

    out_file_hoft   = f"{params.hoft_frame_prefix}-{int(params.t0[i])}-{int(params.duration[i])}.gwf"
    out_file_detchar= f"{params.detchar_frame_prefix}-{int(params.t0[i])}-{int(params.duration[i])}.gwf"

    data = dc.timeseries.TimeSeriesDataset()
    data.download_data(channels = target_channel, 
                       t0 = params.t0[i], duration = params.duration[i], 
                       fs = params.fs, resample_data = True)

    if data.TimeseriesDict is not None:
        data.TimeseriesDict.write(os.path.join(params.out_dir, out_file_hoft))
    
    
    data = dc.timeseries.TimeSeriesDataset()
    data.download_data(channels = witness_channels_all_layers, 
                       t0 = params.t0[i], duration = params.duration[i], 
                       fs = params.fs, resample_data = False)

    if data.TimeseriesDict is not None:
        data.TimeseriesDict.write(os.path.join(params.out_dir, out_file_detchar))


