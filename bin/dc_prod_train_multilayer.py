#!/usr/bin/env python

import os
import pickle
import argparse
import logging.config

import numpy as np

import gwpy
from gwpy.timeseries import TimeSeries, TimeSeriesDict

import glob
import torch
import torch.optim as optim
from torch.utils.data import DataLoader

import deepclean_prod as dc
import deepclean_prod.config as config

# Default tensor type
torch.set_default_tensor_type(torch.FloatTensor)

def parse_cmd():
    
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
        
    # dataset arguments
    parser.add_argument('--train-t0', help='GPS of the first sample', type=int)
    parser.add_argument('--train-duration', help='Duration of train/val frame', type=int)
    parser.add_argument('--chanslist', nargs='+', default=[], help='Lists of paths to the channel lists')
    parser.add_argument('--fs', help='Sampling frequency', 
                        default=config.DEFAULT_SAMPLE_RATE, type=float)
    parser.add_argument('--train-frac', help='Training/validation partition', 
                        default=config.DEFAULT_TRAIN_FRAC, type=float)
    
    # preprocess arguments
    parser.add_argument('--filt-fl', nargs='+', default=[], 
                        help='Lists of Bandpass filter low-frequencies for all the layers', type = float)
    parser.add_argument('--filt-fh', nargs='+', default=[], 
                        help='Lists of Bandpass filter high-frequencies for all the layers', type = float)
    parser.add_argument('--filt-order', help='Bandpass filter order', 
                        default=config.DEFAULT_FORDER, type=int)
    
    # timeseries arguments
    parser.add_argument('--train-kernel', help='Length of each segment in seconds', 
                        default=config.DEFAULT_TRAIN_KERNEL, type=float)
    parser.add_argument('--train-stride', help='Stride between segments in seconds', 
                        default=config.DEFAULT_TRAIN_STRIDE, type=float)
    parser.add_argument('--pad-mode', help='Padding mode', 
                        default=config.DEFAULT_PAD_MODE, type=str)
    
    # training arguments
    parser.add_argument('--batch-size', help='Batch size',
                        default=config.DEFAULT_BATCH_SIZE, type=int)
    parser.add_argument('--max-epochs', help='Maximum number of epochs to train on',
                        default=config.DEFAULT_MAX_EPOCHS, type=int)
    parser.add_argument('--num-workers', help='Number of worker of DataLoader',
                        default=config.DEFAULT_NUM_WORKERS, type=int)
    parser.add_argument('--lr', help='Learning rate of ADAM optimizer', 
                        default=config.DEFAULT_LR, type=float)
    parser.add_argument('--weight-decay', help='Weight decay of ADAM optimizer',
                        default=config.DEFAULT_WEIGHT_DECAY, type=float)
       
    
    # loss function arguments
    parser.add_argument('--fftlength', help='FFT length of loss PSD',
                        default=config.DEFAULT_FFT_LENGTH, type=float)
    parser.add_argument('--overlap', help='Overlapping of loss PSD',
                        default=config.DEFAULT_OVERLAP, type=float)

    parser.add_argument('--psd-weight', help='PSD weight of composite loss',
                        default=config.DEFAULT_PSD_WEIGHT, type=float)
    parser.add_argument('--mse-weight', help='MSE weight of composite',
                        default=config.DEFAULT_MSE_WEIGHT, type=float)
    parser.add_argument('--cross-psd-weight', help='Cross-edge PSD weight of comp. loss',
                        default=config.DEFAULT_CROSS_PSD_WEIGHT, type=float)
    parser.add_argument('--edge-weight', help='edge weight of composite',
                        default=config.DEFAULT_EDGE_WEIGHT, type=float)
    parser.add_argument('--edge-frac', help='fraction of the segment considered as edge',
                        default=config.DEFAULT_EDGE_FRAC, type=float)
   
    # input/output arguments
    parser.add_argument('--train-dir', nargs='+', default=[], 
                        help='lists of paths to the training directories of all layers')
        
    parser.add_argument('--hoft-frame-training', type = str, default=None,
                        help='Path to h(t) frame for training for the first layer')

    parser.add_argument('--detchar-frame-training', type = str, default=None,
                        help='Path to w(t) frame for training for all layers (in one frame)')

    parser.add_argument('--hoft-frame-validation', type = str, default=None,
                        help='Path to h(t) frame for validation for the first layer')

    parser.add_argument('--detchar-frame-validation', type = str, default=None, 
                        help='Path to w(t) frame for validation for all layers (in one frame)')

    parser.add_argument('--hoft-path', default='.', type = str,
                        help='Path to h(t) frames for layer 1. Only needed if any of the \
                        [--hoft-frame-training, --hoft-frame-validation, \
                        --detchar-frame-training --detchar-frame-validation] are not given ')

    parser.add_argument('--detchar-path', help='Path to detchar frames. Only needed if any of the \
                        [--hoft-frame-training, --hoft-frame-validation, \
                        --detchar-frame-training --detchar-frame-validation] are not given ', 
                        default='.', type=str)

    parser.add_argument('--hoft-frame-prefix', default='H-H1_HOFT',
                        help='h(t) frame prefixs excluding the start time and duration. \
                        Only needed if any of the [--hoft-frame-training, --hoft-frame-validation, \
                        --detchar-frame-training --detchar-frame-validation] are not given ', type = str)
    
    parser.add_argument('--detchar-frame-prefix', default='H-H1_Detchar',
                        help='detchar frame prefix excluding the \
                        start time and duration. Only needed if any of the \
                        [--hoft-frame-training, --hoft-frame-validation, \
                        --detchar-frame-training --detchar-frame-validation] are not given ', type=str)
    
    parser.add_argument('--out-channel', nargs='+', default=[], 
                        help='List of names of the output h(t) channel for each layer')
    
    parser.add_argument('--initial-checkpoint', help='pretrained model to initialize with', 
                        default=None, type=str)
    parser.add_argument('--log', help='Log file', type=str)
    
    # cuda arguments
    parser.add_argument('--device', help='Device to use', 
                         default=config.DEFAULT_DEVICE, type=str)
    
    params = parser.parse_args()
    return params

def read_data (params, fname_hoft, fname_detchar, target_channel, wit_channels, 
               start, end, layer = 0, hoft_data = None):
    """
    Read data from the frames in the format as required by the network
    
    """
    data = dc.timeseries.TimeSeriesSegmentDataset(
        params.train_kernel, params.train_stride, params.pad_mode)
    

    wit_channels, fake_chans = data.categorize_channels (wit_channels)

    if hoft_data is None:
        frames  = TimeSeriesDict.read(fname_hoft,    channels = target_channel, start = start, end = end)
    else:
        frames  = TimeSeriesDict()
        frames[target_channel[0]] = hoft_data
    
    detchar = TimeSeriesDict.read(fname_detchar, channels = wit_channels, start = start, end = end)
    frames.append(detchar)
    
    if len(fake_chans) > 0:
        ## t0 and duration from the real frames

        frames = data.add_fake_sinusoids (frames, fake_chans, 
                                          t0 = start, 
                                          duration = end - start, fs = params.fs)
    
    frames = frames.resample(params.fs)

    # sorted by channel name
    frames = dc.timeseries.OrderedDict(sorted(frames.items()))
    
    # reset attributes 
    dataset = []
    chans = []
    for chan, ts in frames.items():
        if np.mean(abs(ts.value)) > 0.0: 
            dataset.append(ts.value)
            chans.append(chan)
    dataset = np.stack(dataset)
    chans = np.stack(chans)

    data.data = dataset
    data.channels = chans
    data.t0 = start
    data.fs = params.fs
    data.target_idx = np.where(data.channels == target_channel)[0][0]  
    return data


def preprocess_and_predict (data, params, model, ppr, device):
    preprocessed = data.bandpass(ppr['filt_fl'], ppr['filt_fh'], 
                                 ppr['filt_order'], 'target')
    
    preprocessed = preprocessed.normalize(ppr['mean'], ppr['std'])

    # Load preprocessed data into dataloader
    data_loader = DataLoader(
        preprocessed, batch_size=config.DEFAULT_BATCH_SIZE,
        num_workers=config.DEFAULT_NUM_WORKERS, shuffle=False)

    target = data.get_target()  # get raw data

    # predict noise from auxilliary channels
    pred_batches = dc.nn.utils.evaluate(data_loader, model, device=device)

    # post-processing the prediction
    # overlap-add to join together prediction
    noverlap = int((preprocessed.kernel - preprocessed.stride) * preprocessed.fs)
    pred = dc.signal.overlap_add(pred_batches, noverlap, config.DEFAULT_WINDOW)
    # convert back to unit of target
    pred *= ppr['std'][preprocessed.target_idx].ravel()
    pred += ppr['mean'][preprocessed.target_idx].ravel()
    # apply bandpass filter
    pred = dc.signal.filter_add(pred, preprocessed.fs, ppr['filt_fl'], ppr['filt_fh'],
                                ppr['filt_order'])
    # because of data padding, we apply a cut to the output
    pred = pred[:target.size]
    return pred
    



params =  parse_cmd()
pickle.dump({"params": params}, open("dc_prod_train_params.p", "wb"))
params = pickle.load(open("dc_prod_train_params.p", "rb"))['params']


nlayers = len(params.chanslist)

#########   make subdirs for training if not specified   #########
if len(params.train_dir) == 1 and nlayers > 1:
	train_dir_list = []
	for i in range(nlayers):
		train_dir_layer_i = os.path.join(params.train_dir[0], f"train_layer_{i+1}")
		train_dir_list.append(train_dir_layer_i) 
	
	params.train_dir = train_dir_list
#-----------------------------------------------------------------




# Use GPU if available
device = dc.nn.utils.get_device(params.device)

# partition of data into train and val
train_t0 = params.train_t0
train_duration = int(params.train_duration * params.train_frac)
val_t0 = train_t0 + train_duration
val_duration = params.train_duration - train_duration


# Get data from NDS server or local
train_data = dc.timeseries.TimeSeriesSegmentDataset(
    params.train_kernel, params.train_stride, params.pad_mode)
val_data = dc.timeseries.TimeSeriesSegmentDataset(
    params.train_kernel, params.train_stride, params.pad_mode)


cleaned_train = None
cleaned_val   = None

if params.hoft_frame_training is None:
    frame_string = "{}-*-*.gwf".format(params.hoft_frame_prefix)
    params.hoft_frame_training = glob.glob(os.path.join(params.hoft_path, frame_string))

if params.detchar_frame_training is None:
    frame_string = "{}-*-*.gwf".format(params.detchar_frame_prefix)
    params.detchar_frame_training = glob.glob(os.path.join(params.detchar_path, frame_string))

if params.hoft_frame_validation is None:
    frame_string = "{}-*-*.gwf".format(params.hoft_frame_prefix)
    params.hoft_frame_validation = glob.glob(os.path.join(params.hoft_path, frame_string))

if params.detchar_frame_validation is None:
    frame_string = "{}-*-*.gwf".format(params.detchar_frame_prefix)
    params.detchar_frame_validation = glob.glob(os.path.join(params.detchar_path, frame_string))


for layer in range(nlayers):
    # Create output directory
    os.makedirs(params.train_dir[layer], exist_ok=True)
    if params.log is not None:
        params.log = os.path.join(params.train_dir[layer], params.log)
    logging.basicConfig(filename=params.log, filemode='a', 
                        format='%(asctime)s - %(message)s', level=logging.DEBUG)
    logging.info('Create training directory: {}'.format(params.train_dir[layer]))

    
    channels = open(params.chanslist[layer]).read().splitlines()
    
    target_channel = channels[:1]
    wit_channels   = channels[1:]
    n_witchans = len(wit_channels)
        
    train_data = read_data (params = params, 
                    fname_hoft     = params.hoft_frame_training, 
                    fname_detchar  = params.detchar_frame_training, 
                    target_channel = target_channel, 
                    wit_channels   = wit_channels, 
                    start          = train_t0, 
                    end            = train_t0 + train_duration, 
                    layer = layer, 
                    hoft_data = cleaned_train)

    val_data = read_data (params   = params, 
                    fname_hoft     = params.hoft_frame_validation, 
                    fname_detchar  = params.detchar_frame_validation, 
                    target_channel = target_channel, 
                    wit_channels   = wit_channels, 
                    start          = val_t0, 
                    end            = val_t0 + val_duration, 
                    layer = layer, 
                    hoft_data = cleaned_val)

    # Preprocess data
    logging.info('Preprocessing')
    # bandpass filter
    train_data_prep = train_data.bandpass(
        params.filt_fl[layer], params.filt_fh[layer], params.filt_order, 'target')
    val_data_prep = val_data.bandpass(
        params.filt_fl[layer], params.filt_fh[layer], params.filt_order, 'target')

    # normalization
    mean = train_data_prep.mean
    std = train_data_prep.std
    train_data_prep = train_data_prep.normalize()
    val_data_prep = val_data_prep.normalize(mean, std)

    # Save preprocessing setting
    ppr = { 'mean': mean,
            'std': std,
            'filt_fl': params.filt_fl,
            'filt_fh': params.filt_fh,
            'filt_order': params.filt_order,
            'channels' : list(train_data.channels),
            'target'   : train_data.channels[train_data.target_idx],
            'wit_channels' : list(np.delete(train_data.channels, train_data.target_idx)),
            'n_witness_channels' : train_data.channels.shape[0] - 1
        }
    with open(os.path.join(params.train_dir[layer], 'ppr.bin'), 'wb') as f:
        pickle.dump(ppr, f, protocol=-1)

    # Read dataset into DataLoader
    train_loader = DataLoader(
        train_data_prep, params.batch_size, num_workers=params.num_workers)
    val_loader = DataLoader(
        val_data_prep, params.batch_size, num_workers=params.num_workers)

    # Creating model, loss function, optimizer and lr scheduler
    logging.info('Creating neural network, loss function, optimizer, etc.')
    model = dc.nn.net.DeepClean(train_data_prep.n_channels - 1)
    model = model.to(device)  # transfer model to GPU if available
    logging.info(model)


    # initialize the weights from a pre-trained model checkpoint
    if params.initial_checkpoint is not None:
        try:
            logging.info('Initializing from the checkpoint: {}'.format(params.initial_checkpoint))
            model.load_state_dict(torch.load(params.initial_checkpoint, map_location=device))
        except:
            logging.info('Could not load the given checkpoint. Training from scratch . . .')
            pass

    criterion = dc.criterion.CompositePSDLoss(
        params.fs, params.filt_fl[layer], params.filt_fh[layer],
        fftlength=params.fftlength, overlap=params.overlap, 
        psd_weight=params.psd_weight, mse_weight=params.mse_weight, 
        edge_weight=params.edge_weight, edge_frac = params.edge_frac, 
        cross_psd_weight = params.cross_psd_weight,
        train_kernel = params.train_kernel, batch_size = params.batch_size, 
        train_stride = params.train_stride,
        reduction='sum', device=device)

    optimizer = optim.Adam(
        model.parameters(), lr=params.lr, weight_decay=params.weight_decay)

    lr_scheduler = optim.lr_scheduler.StepLR(optimizer, 10, 0.1)

    # Start training
    train_logger = dc.logger.Logger(outdir=params.train_dir[layer], metrics=['loss'])
    dc.nn.utils.train(
        train_loader, model, criterion, optimizer, lr_scheduler,
        val_loader=val_loader, max_epochs=params.max_epochs, logger=train_logger, 
        device=device)
    
    
    logging.info(f'Cleaning the training dataset for layer {layer+1} . .')
    checkpoint = dc.nn.utils.get_last_checkpoint(
            os.path.join(params.train_dir[layer], 'models'))
    model.load_state_dict(torch.load(checkpoint, map_location=device))
    
    pred  = preprocess_and_predict (train_data, params, model, ppr, device)
    clean = train_data.get_target()  - pred
    cleaned_train  = TimeSeries(clean, t0=train_data.t0, sample_rate=train_data.fs, 
            channel=params.out_channel[layer], name=params.out_channel[layer])
    
    pred  = preprocess_and_predict (val_data, params, model, ppr, device)
    clean = val_data.get_target()  - pred
    cleaned_val  = TimeSeries(clean, t0=val_data.t0, sample_rate=val_data.fs, 
            channel=params.out_channel[layer], name=params.out_channel[layer])
    
    cleaned_train.write(os.path.join(
        params.train_dir[layer], "training_data_cleaned_layer_{:d}.gwf".format(layer)))
    cleaned_val.write(os.path.join(
        params.train_dir[layer], "validation_data_cleaned_layer_{:d}.gwf".format(layer)))


