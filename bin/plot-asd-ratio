#!/usr/bin/env python

import numpy as np
import deepclean_prod as dc
utils = dc.pp_tools_deepClean

from gwpy.timeseries import TimeSeries
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import pickle
import os
import argparse
import logging
import logging.config

import glob

import gwpy
from gwpy.timeseries import TimeSeries, TimeSeriesDict


def parse_cmd():

    
    parser = argparse.ArgumentParser(
        prog=os.path.basename(__file__), usage='%(prog)s [options]')
    
    parser.add_argument('--t0', help='GPS time',  type=int)

    parser.add_argument('--t1', help='GPS of the last sample.', default=None , type=int)

    parser.add_argument('--duration', default=1024 , type=float, 
                        help='Duration of data to be plotted')

    parser.add_argument('--frame-org', help='Original frame',  type=str)
    parser.add_argument('--frame-dc', help='DeepClean frame',  type=str)

    parser.add_argument('--channel-org', help='Original channel',  type=str)
    parser.add_argument('--channel-dc', help='DeepClean channel',  type=str)
    
    parser.add_argument('--f-min', help='Minimum frequency',  type=float)
    parser.add_argument('--f-max', help='Maximum frequency',  type=float)
    
    parser.add_argument('--fig-name', help='Figure name',  type=str)

    params = parser.parse_args()
    
    ''' example:
    plot-asd-ratio --t0 1238172987 --duration 3031 --frame-org gated_outdir/H-H1_HOFT_DC-1238172986-3032/H-H1_HOFT-1238172987-3031.gwf --frame-dc gated_outdir/H-H1_HOFT_DC-1238172986-3032/H-H1_HOFT_DC_NOGATE_60Hz-1238172987-3031.gwf --channel-org H1:GDS-CALIB_STRAIN --channel-dc H1:GDS-CALIB_STRAIN_DC_60Hz --f-min 55 --f-max 65 --fig-name /home/muhammed.saleem/public_html/LSC/deepclean/multilayer_test/asdr_layer1_nogate.pdf
    '''
    
    return params

def create_outdir_if_not_exists (fig_name):
    
    savepath = fig_name.split(fig_name.split("/")[-1])[0]
    if not os.path.exists (savepath):
        os.makedirs(savepath, exist_ok = True)


params =  parse_cmd()
pickle.dump({"params": params}, open("plot_asd_ratio.p", "wb"))
params = pickle.load(open("plot_asd_ratio.p", "rb"))['params']


if params.t1 is None:
    params.t1 = params.t0 + params.duration


if params.frame_org.split(".")[-1] == "gwf":    
    TS_org = TimeSeries.read(params.frame_org, channel = params.channel_org, 
                         start = params.t0, end = params.t1)

if params.frame_org.split(".")[-1] == "h5":    
    TS_org = utils.timeSeries_from_h5 (params.frame_org, channel =  params.channel_org).crop(
                                    params.t0, end = params.t1)
    
    
if params.frame_dc.split(".")[-1] == "gwf":    
    TS_dc = TimeSeries.read(params.frame_dc, channel = params.channel_dc, 
                         start = params.t0, end = params.t1)

if params.frame_dc.split(".")[-1] == "h5":    
    TS_dc = utils.timeSeries_from_h5 (params.frame_dc, channel =  params.channel_dc).crop(
                                    params.t0, end = params.t1)

create_outdir_if_not_exists (params.fig_name)

utils.plot_dc_vs_org_asd_ratio (TS_dc, TS_org, params.fig_name, 
                              fmin=params.f_min, fmax=params.f_max)




dnsdomain = os.popen('dnsdomainname').read().split("\n")[0]
url = "https://ldas-jobs.{}{}".format(dnsdomain, params.fig_name.replace("public_html/", ""))
url = url.replace("/home/","/~")
print (f'Plot created at: \n {url}')