import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import os
import pickle
import h5py
from gwpy.timeseries import TimeSeries



def timeSeries_from_h5 (filename, 
                        channel = 'H1:GDS-CALIB_STRAIN' ):

    """
    To read the .h5 output files produced by dc-prod-clean
    this format is used for storing the original (unclean) data
    NB: Cleaned data are stored as frame files

    Parameters
    ----------
    filename : `str`
        path to the h5 file to be read
    channel  : `str`
        default is 'H1:GDS-CALIB_STRAIN'
    
    Returns:
    --------
    data_ts : `gwpy.timeseries.TimeSeries`
        the data in the timeseries format.

    """

    f = h5py.File(filename, 'r')
    channels = list(f.keys())

    self_channels = []
    self_data = []
    with h5py.File(filename, 'r') as f:
        fobj = f
        for chan, data in fobj.items():
            if chan not in channels:
                continue
            self_channels.append(chan)
            self_data.append(data[:])
            t0 = data.attrs['t0']
            fs = data.attrs['sample_rate']

    data_ndarray = self_data[self_channels.index(channel)]
    data_ts = TimeSeries(data_ndarray, t0=t0, sample_rate=fs, name=channel, unit="ct", channel=channel)
    
    return data_ts



def plot_asd_from_frames (framefiles, channels, figurename, fmin=20, fmax=300, labels = None):
    """
    Plots the asd of the data from the framefile(s)
    This plots all in one panel. Good when comparing the ASDs from different detectors
    
    Parameters
    ----------
    
    framefiles : `list`
        list of framefiles

    channels : `list` of `str`or `~gwpy.detector.Channel`
        list of channels
        the size of the list should be the same as that of the framefiles list.
    
    fmin : `float`
        defaults to 20Hz 
    
    fmax : `float`
        defaults to 300Hz 

    """
    if type(framefiles) is not list:
        framefiles = [framefiles]

    if type(channels) is not list:
        channels = [channels]
    
    if labels == None:
        labels = channels
    
    default_styles = ['solid','dashed','dotted', 'dashdot' ]
    styles = default_styles[:len(framefiles)]

    # make plots by looping over the frames
    plt.figure(figsize=(6,6))
    for filename, channel, label, style in zip(framefiles, channels, labels, styles):
        TS = TimeSeries.read(filename, channel)
        asd = TS.asd(fftlength=10,overlap=0.5,method='median')
        plt.loglog(asd.crop(fmin,fmax),label=label, ls=style)
    plt.legend()
    plt.grid(True)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel(r'ASD [Hz$^{-1/2}$]')
    #plt.savefig(os.path.join(figure_directory,figurename)) 
    
    
def plot_asd_from_TS (TS_list, figurename=None, fmin=20, fmax=300, labels = None):
    """
    Plots the asd of the data from the framefile(s)
    This plots all in one panel. Good when comparing the ASDs from different detectors
    
    Parameters
    ----------
    
    TS_list : `list`
        list of timeserieses
    
    fmin : `float`
        defaults to 20Hz 
    
    fmax : `float`
        defaults to 300Hz 

    """
    
    default_styles = ['solid','dashed','dotted', 'dashdot' ]
    styles = default_styles[:len(TS_list)]

    # make plots by looping over the frames
    plt.figure(figsize=(6,4))
    for TS, label, style in zip(TS_list, labels, styles):
        asd = TS.asd(fftlength=10,overlap=0.5,method='median')
        plt.loglog(asd.crop(fmin,fmax),label=label, ls='solid', lw= 2)
    plt.legend(fontsize=13)
    plt.grid(True)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel(r'ASD [Hz$^{-1/2}$]')
    plt.tight_layout()
    if not figurename == None:
        plt.savefig(figurename) 
    
    
def plot_dc_vs_org_asd_ratio (TS_dc, TS_org, figurename, fmin=20, fmax=300):

    
    asd_dc  = TS_dc.asd(fftlength=10,overlap=5,method='median')
    asd_org = TS_org.asd(fftlength=10,overlap=5,method='median')
    
    plt.figure(figsize=(6,4))
    plt.semilogy(asd_dc.crop(fmin,fmax)/asd_org.crop(fmin,fmax), lw=2.8, ls='solid', color='green')


    plt.grid(True)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel(r'ASD Ratio [DeepClean/Original]')
    plt.tight_layout()
    plt.savefig(figurename) 
    
    
def plot_spectrogram(TS,  figurename, label='DeepClean'):
    specgram = TS.spectrogram(100, fftlength=8, overlap=4) ** (1/2.)
    plot = specgram.plot(norm='log', vmin=1e-23, vmax=1e-20)
    ax = plot.gca()
    ax.set_ylim(50, 70)
    ax.set_yscale('log')
    ax.colorbar(label='GW strain ASD [strain/$\sqrt{\mathrm{Hz}}$]')
    plt.title(label)
    plt.savefig(figurename) 

    
def mean_asd_ratio (asd1, asd2, fmin = 58.0, fmax=62.0):
    """
    Computes the average of the asd ratios (asd1/asd2) between fmin and fmax 
    
    Parameters
    ----------
    
    asd1 : `gwpy.frequencyseries.frequencyseries.FrequencySeries`

    asd2 : `gwpy.frequencyseries.frequencyseries.FrequencySeries`

    fmin : `float`
        defaults to 58Hz as used for 60Hz subtraction studies
    
    fmax : `float`
        defaults to 62Hz as used for 60Hz subtraction studies
    
    """
    
    asd_ratio = np.array(asd1.crop(fmin, fmax)/asd2.crop(fmin, fmax))
    return np.sum(asd_ratio)/len(asd_ratio)


def get_epoch_vs_asd_ratio_from_files (file_org, 
                            file_dc, 
                            channel_org, 
                            channel_dc,
                            tstart = None, 
                            duration = None, 
                            fmin=58.0, 
                            fmax=62.0,
                            fftlen = 10,
                            fft_overlap = 0.0,
                            asd_method = 'median'):
    """
    computes the mean asd ratio given the original filename and frequency windows as inputs
    
    Parameters
    ----------
    
    file_org : `str`
        file name or path to the original .h5 file
    cleaned_files : `list`
        a list of cleaned files from which the cleaned version of the 
        file_org will be found out
    
    """
    if tstart == None and duration == None:
        # if not given, take those from file names
        tstart   = int(file_dc.split(".")[-2].split("-")[-2])
        duration = int(file_dc.split(".")[-2].split("-")[-1])

    epoch = tstart + 0.5 * duration

    darm_org_ts = timeSeries_from_h5 (file_org, channel_org)
    darm_dpc_ts = TimeSeries.read(file_dc, channel_dc)

    asd_org = darm_org_ts.asd(fftlength=fftlen,overlap=fft_overlap,method=asd_method)
    asd_dpc = darm_dpc_ts.asd(fftlength=fftlen,overlap=fft_overlap,method=asd_method)

    MeanAsdRatio = mean_asd_ratio (asd_dpc, asd_org, fmin=fmin, fmax= fmax)

    return epoch, MeanAsdRatio


